﻿using AutoMapper;
using Marfrig.MRCG.Domain.Contracts.AppServices;
using Marfrig.MRCG.Domain.Models;
using Marfrig.MRCG.Domain.ViewModels;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace Marfrig.MRCG.Web.Controllers
{
    public class PecuaristaController : Controller
    {
        private readonly IPecuaristaAppService _pecuaristaAppService;

        public PecuaristaController(IPecuaristaAppService pecuaristaAppService)
        {
            _pecuaristaAppService = pecuaristaAppService;
        }

        public ActionResult Index()
        {
            var listaPecuaristas = Mapper.Map<IEnumerable<Pecuarista>, IEnumerable<PecuaristaViewModel>>(_pecuaristaAppService.GetAll());
            return View(listaPecuaristas);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PecuaristaViewModel pecuarista = Mapper.Map<Pecuarista, PecuaristaViewModel>(_pecuaristaAppService.GetById((int)id));
            if (pecuarista == null)
            {
                return HttpNotFound();
            }
            return View(pecuarista);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PecuaristaViewModel pecuaristaViewModel)
        {
            if (ModelState.IsValid)
            {
                _pecuaristaAppService.Create(Mapper.Map<PecuaristaViewModel, Pecuarista>(pecuaristaViewModel));
                return RedirectToAction("Index");
            }

            return View(pecuaristaViewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PecuaristaViewModel pecuarista = Mapper.Map<Pecuarista, PecuaristaViewModel>(_pecuaristaAppService.GetById((int)id));
            if (pecuarista == null)
            {
                return HttpNotFound();
            }
            return View(pecuarista);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PecuaristaViewModel pecuaristaViewModel)
        {
            if (ModelState.IsValid)
            {
                Pecuarista pecuarista = _pecuaristaAppService.GetById(pecuaristaViewModel.PecuaristaId);
                pecuarista.Nome = pecuaristaViewModel.Nome;
                _pecuaristaAppService.Update(pecuarista);
                return RedirectToAction("Index");
            }
            return View(pecuaristaViewModel);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PecuaristaViewModel pecuarista = Mapper.Map<Pecuarista, PecuaristaViewModel>(_pecuaristaAppService.GetById((int)id));
            if (pecuarista == null)
            {
                return HttpNotFound();
            }
            return View(pecuarista);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pecuarista pecuarista = _pecuaristaAppService.GetById((int)id);
            _pecuaristaAppService.Remove(pecuarista);
            return RedirectToAction("Index");
        }
    }
}