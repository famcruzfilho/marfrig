﻿using AutoMapper;
using Marfrig.MRCG.Domain.Contracts.AppServices;
using Marfrig.MRCG.Domain.Models;
using Marfrig.MRCG.Domain.ViewModels;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace Marfrig.MRCG.Web.Controllers
{
    public class AnimalController : Controller
    {
        private readonly IAnimalAppService _animalAppService;

        public AnimalController(IAnimalAppService animalAppService)
        {
            _animalAppService = animalAppService;
        }

        public ActionResult Index()
        {
            var listaAnimais = Mapper.Map<IEnumerable<Animal>, IEnumerable<AnimalViewModel>>(_animalAppService.GetAll());
            return View(listaAnimais);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnimalViewModel animal = Mapper.Map<Animal, AnimalViewModel>(_animalAppService.GetById((int)id));
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AnimalViewModel animalViewModel)
        {
            if (ModelState.IsValid)
            {
                _animalAppService.Create(Mapper.Map<AnimalViewModel, Animal>(animalViewModel));
                return RedirectToAction("Index");
            }

            return View(animalViewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnimalViewModel animal = Mapper.Map<Animal, AnimalViewModel>(_animalAppService.GetById((int)id));
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AnimalViewModel animalViewModel)
        {
            if (ModelState.IsValid)
            {
                Animal animal = _animalAppService.GetById(animalViewModel.AnimalId);
                animal.Descricao = animalViewModel.Descricao;
                animal.Preco = animalViewModel.Preco;
                _animalAppService.Update(animal);
                return RedirectToAction("Index");
            }
            return View(animalViewModel);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnimalViewModel animal = Mapper.Map<Animal, AnimalViewModel>(_animalAppService.GetById((int)id));
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Animal animal = _animalAppService.GetById((int)id);
            _animalAppService.Remove(animal);
            return RedirectToAction("Index");
        }
    }
}