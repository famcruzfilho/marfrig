﻿using System.Web.Mvc;

namespace Marfrig.MRCG.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}