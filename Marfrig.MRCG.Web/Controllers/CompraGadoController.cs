﻿using AutoMapper;
using Marfrig.MRCG.Domain.Contracts.AppServices;
using Marfrig.MRCG.Domain.Models;
using Marfrig.MRCG.Domain.ViewModels;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace Marfrig.MRCG.Web.Controllers
{
    public class CompraGadoController : Controller
    {
        private readonly ICompraGadoAppService _compraGadoAppService;
        private readonly IPecuaristaAppService _pecuaristaAppService;

        public CompraGadoController(ICompraGadoAppService compraGadoAppService, IPecuaristaAppService pecuaristaAppService)
        {
            _compraGadoAppService = compraGadoAppService;
            _pecuaristaAppService = pecuaristaAppService;
        }

        public ActionResult Index()
        {
            ViewBag.Pecuaristas = new SelectList(_pecuaristaAppService.GetAll(), "PecuaristaId", "Nome", "Selecione");
            var listaComprasGado = Mapper.Map<IEnumerable<CompraGado>, IEnumerable<CompraGadoViewModel>>(_compraGadoAppService.GetAllComplete());

            return View(listaComprasGado);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompraGadoViewModel compraGado = Mapper.Map<CompraGado, CompraGadoViewModel>(_compraGadoAppService.GetById((int)id));
            if (compraGado == null)
            {
                return HttpNotFound();
            }

            return View(compraGado);
        }

        public ActionResult Create()
        {
            var pecuaristas = _pecuaristaAppService.GetAll();
            ViewBag.PecuaristaId = new SelectList(pecuaristas, "PecuaristaId", "Nome");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompraGadoViewModel compraGadoViewModel)
        {
            if (ModelState.IsValid)
            {
                _compraGadoAppService.Create(Mapper.Map<CompraGadoViewModel, CompraGado>(compraGadoViewModel));
                return RedirectToAction("Index");
            }
            var pecuaristas = _pecuaristaAppService.GetAll();
            ViewBag.PecuaristaId = new SelectList(pecuaristas, "PecuaristaId", "Nome", compraGadoViewModel.PecuaristaId);

            return View(compraGadoViewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompraGadoViewModel compraGadoViewModel = Mapper.Map<CompraGado, CompraGadoViewModel>(_compraGadoAppService.GetById((int)id));
            if (compraGadoViewModel == null)
            {
                return HttpNotFound();
            }
            var pecuaristas = _pecuaristaAppService.GetAll();
            ViewBag.PecuaristaId = new SelectList(pecuaristas, "PecuaristaId", "Nome", compraGadoViewModel.PecuaristaId);
            return View(compraGadoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompraGadoViewModel compraGadoViewModel)
        {
            if (ModelState.IsValid)
            {
                CompraGado compraGado = _compraGadoAppService.GetById(compraGadoViewModel.CompraGadoId);
                compraGado.PecuaristaId = compraGadoViewModel.PecuaristaId;
                compraGado.DataEntrega = compraGadoViewModel.DataEntrega;
                _compraGadoAppService.Update(compraGado);
                return RedirectToAction("Index");
            }
            var pecuaristas = _pecuaristaAppService.GetAll();
            ViewBag.PecuaristaId = new SelectList(pecuaristas, "PecuaristaId", "Nome", compraGadoViewModel.PecuaristaId);
            return View(compraGadoViewModel);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompraGadoViewModel compraGadoViewModel = Mapper.Map<CompraGado, CompraGadoViewModel>(_compraGadoAppService.GetById((int)id));
            if (compraGadoViewModel == null)
            {
                return HttpNotFound();
            }
            return View(compraGadoViewModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CompraGado compraGado = _compraGadoAppService.GetById((int)id);
            _compraGadoAppService.Remove(compraGado);
            return RedirectToAction("Index");
        }
    }
}