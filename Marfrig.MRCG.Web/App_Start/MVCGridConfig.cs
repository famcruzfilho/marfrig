[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Marfrig.MRCG.Web.MVCGridConfig), "RegisterGrids")]

namespace Marfrig.MRCG.Web
{
    using AutoMapper;
    using Marfrig.MRCG.Domain.Contracts.AppServices;
    using Marfrig.MRCG.Domain.Models;
    using Marfrig.MRCG.Domain.ViewModels;
    using MVCGrid.Models;
    using MVCGrid.Web;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    public static class MVCGridConfig 
    {
        public static void RegisterGrids()
        {
            MVCGridDefinitionTable
                .Add("CompraGadoGrid", new MVCGridBuilder<CompraGadoViewModel>()
                .WithAuthorizationType(AuthorizationType.AllowAnonymous)
                .AddColumns(cols =>
                {
                    cols.Add("Id").WithValueExpression(p => p.CompraGadoId.ToString());
                    cols.Add("Pecuarista").WithHeaderText("Pecuarista").WithValueExpression(p => p.Pecuarista.Nome);
                    cols.Add("DataEntrega").WithHeaderText("Data de Entrega").WithValueExpression(p => p.DataEntrega.ToShortDateString());
                })
                .WithRetrieveDataMethod((context) =>
                {
                    var options = context.QueryOptions;
                    var _compraGadoAppService = DependencyResolver.Current.GetService<ICompraGadoAppService>();
                    var items = _compraGadoAppService.GetAllComplete();
                    return new QueryResult<CompraGadoViewModel>()
                    {
                        Items = Mapper.Map<IEnumerable<CompraGado>, IEnumerable<CompraGadoViewModel>>(items),
                        TotalRecords = items.ToList().Count()
                    };
                }));
        }
    }
}