﻿using AutoMapper;

namespace Marfrig.MRCG.Crosscutting.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<DomainToDTOMappingProfile>();
            });
        }
    }
}