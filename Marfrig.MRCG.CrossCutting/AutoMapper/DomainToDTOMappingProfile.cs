﻿using AutoMapper;
using Marfrig.MRCG.Domain.DTOs;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.Crosscutting.AutoMapper
{
    public class DomainToDTOMappingProfile : Profile
    {
        protected void Configure()
        {
            CreateMap<Animal, AnimalDTO>().ReverseMap();
            CreateMap<CompraGadoItem, CompraGadoItemDTO>().ReverseMap();
            CreateMap<CompraGado, CompraGadoDTO>().ReverseMap();
            CreateMap<Pecuarista, PecuaristaDTO>().ReverseMap();
        }
    }
}