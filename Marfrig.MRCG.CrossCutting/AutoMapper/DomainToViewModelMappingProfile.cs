﻿using AutoMapper;
using Marfrig.MRCG.Domain.Models;
using Marfrig.MRCG.Domain.ViewModels;

namespace Marfrig.MRCG.Crosscutting.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        protected void Configure()
        {
            CreateMap<Animal, AnimalViewModel>().ReverseMap();
            CreateMap<CompraGadoItem, CompraGadoItemViewModel>().ReverseMap();
            CreateMap<CompraGado, CompraGadoViewModel>().ReverseMap();
            CreateMap<Pecuarista, PecuaristaViewModel>().ReverseMap();
        }
    }
}