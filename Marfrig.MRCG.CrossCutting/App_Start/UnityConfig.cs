using Marfrig.MRCG.AppService.AppServices;
using Marfrig.MRCG.Business.Services;
using Marfrig.MRCG.Domain.Contracts.AppServices;
using Marfrig.MRCG.Domain.Contracts.Repositories;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;
using Marfrig.MRCG.Infraestructure.Data.Context;
using Marfrig.MRCG.Infraestructure.Data.Repositories;
using System;
using Unity;

namespace Marfrig.MRCG.Crosscutting
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            //Contexto
            container.RegisterType<DataContext, DataContext>();

            //Modelos
            container.RegisterType<Animal, Animal>();
            container.RegisterType<CompraGado, CompraGado>();
            container.RegisterType<CompraGadoItem, CompraGadoItem>();
            container.RegisterType<Pecuarista, Pecuarista>();

            //Application Services
            container.RegisterType<IAnimalAppService, AnimalAppService>();
            container.RegisterType<ICompraGadoAppService, CompraGadoAppService>();
            container.RegisterType<ICompraGadoItemAppService, CompraGadoItemAppService>();
            container.RegisterType<IPecuaristaAppService, PecuaristaAppService>();

            //Services
            container.RegisterType<IAnimalService, AnimalService>();
            container.RegisterType<ICompraGadoService, CompraGadoService>();
            container.RegisterType<ICompraGadoItemService, CompraGadoItemService>();
            container.RegisterType<IPecuaristaService, PecuaristaService>();

            //Repositories
            container.RegisterType<IAnimalRepository, AnimalRepository>();
            container.RegisterType<ICompraGadoRepository, CompraGadoRepository>();
            container.RegisterType<ICompraGadoItemRepository, CompraGadoItemRepository>();
            container.RegisterType<IPecuaristaRepository, PecuaristaRepository>();
        }
    }
}