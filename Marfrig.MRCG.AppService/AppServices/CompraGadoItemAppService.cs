﻿using Marfrig.MRCG.AppService.AppServices.Base;
using Marfrig.MRCG.Domain.Contracts.AppServices;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.AppService.AppServices
{
    public class CompraGadoItemAppService : BaseAppService<CompraGadoItem>, ICompraGadoItemAppService
    {
        private readonly ICompraGadoItemService _compraGadoItemService;

        public CompraGadoItemAppService(ICompraGadoItemService compraGadoItemService) : base(compraGadoItemService)
        {
            _compraGadoItemService = compraGadoItemService;
        }
    }
}