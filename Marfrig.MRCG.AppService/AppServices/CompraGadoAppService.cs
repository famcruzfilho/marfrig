﻿using Marfrig.MRCG.AppService.AppServices.Base;
using Marfrig.MRCG.Domain.Contracts.AppServices;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;
using System.Collections.Generic;

namespace Marfrig.MRCG.AppService.AppServices
{
    public class CompraGadoAppService : BaseAppService<CompraGado>, ICompraGadoAppService
    {
        private readonly ICompraGadoService _compraGadoService;

        public CompraGadoAppService(ICompraGadoService compraGadoService) : base(compraGadoService)
        {
            _compraGadoService = compraGadoService;
        }

        public IEnumerable<CompraGado> GetAllComplete()
        {
            return _compraGadoService.GetAllComplete();
        }

        public CompraGado GetByIdComplete(int id)
        {
            return _compraGadoService.GetByIdComplete(id);
        }
    }
}