﻿using Marfrig.MRCG.Domain.Contracts.AppServices.Base;
using Marfrig.MRCG.Domain.Contracts.Services.Base;
using System.Collections.Generic;

namespace Marfrig.MRCG.AppService.AppServices.Base
{
    public class BaseAppService<TEntity> : IBaseAppService<TEntity> where TEntity : class
    {
        private readonly IBaseService<TEntity> _service;

        public BaseAppService(IBaseService<TEntity> service)
        {
            _service = service;
        }

        public void Create(TEntity obj)
        {
            _service.Create(obj);
        }

        public void Create(IEnumerable<TEntity> objs)
        {
            _service.Create(objs);
        }

        public void Dispose()
        {
            _service.Dispose();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _service.GetAll();
        }

        public TEntity GetById(int id)
        {
            return _service.GetById(id);
        }

        public void Remove(TEntity obj)
        {
            _service.Remove(obj);
        }

        public void Update(TEntity obj)
        {
            _service.Update(obj);
        }
    }
}