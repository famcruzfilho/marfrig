﻿using Marfrig.MRCG.AppService.AppServices.Base;
using Marfrig.MRCG.Domain.Contracts.AppServices;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.AppService.AppServices
{
    public class PecuaristaAppService : BaseAppService<Pecuarista>, IPecuaristaAppService
    {
        private readonly IPecuaristaService _pecuaristaService;

        public PecuaristaAppService(IPecuaristaService pecuaristaService) : base(pecuaristaService)
        {
            _pecuaristaService = pecuaristaService;
        }
    }
}