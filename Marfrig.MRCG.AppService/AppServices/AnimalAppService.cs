﻿using Marfrig.MRCG.AppService.AppServices.Base;
using Marfrig.MRCG.Domain.Contracts.AppServices;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.AppService.AppServices
{
    public class AnimalAppService : BaseAppService<Animal>, IAnimalAppService
    {
        private readonly IAnimalService _animalService;

        public AnimalAppService(IAnimalService animalService) : base(animalService)
        {
            _animalService = animalService;
        }
    }
}