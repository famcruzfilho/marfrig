﻿namespace Marfrig.MRCG.Domain.Models
{
    public class Animal
    {
        public int AnimalId { get; set; }
        public string Descricao { get; set; }
        public string Preco { get; set; }
    }
}