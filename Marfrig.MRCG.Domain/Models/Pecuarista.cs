﻿namespace Marfrig.MRCG.Domain.Models
{
    public class Pecuarista
    {
        public int PecuaristaId { get; set; }
        public string Nome { get; set; }
    }
}