﻿using System;

namespace Marfrig.MRCG.Domain.Models
{
    public class CompraGado
    {
        public int CompraGadoId { get; set; }
        public DateTime DataEntrega { get; set; }
        public int PecuaristaId { get; set; }
        public Pecuarista Pecuarista { get; set; }
    }
}