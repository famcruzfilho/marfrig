﻿using Marfrig.MRCG.Domain.Contracts.Services.Base;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.Domain.Contracts.Services
{
    public interface ICompraGadoItemService : IBaseService<CompraGadoItem>
    {

    }
}