﻿using System.Collections.Generic;

namespace Marfrig.MRCG.Domain.Contracts.Services.Base
{
    public interface IBaseService<TEntity> where TEntity : class
    {
        void Create(TEntity obj);
        void Create(IEnumerable<TEntity> objs);
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int id);
        void Remove(TEntity obj);
        void Update(TEntity obj);
        void Dispose();
    }
}