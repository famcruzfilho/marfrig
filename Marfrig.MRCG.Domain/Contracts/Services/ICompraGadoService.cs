﻿using Marfrig.MRCG.Domain.Contracts.Services.Base;
using Marfrig.MRCG.Domain.Models;
using System.Collections.Generic;

namespace Marfrig.MRCG.Domain.Contracts.Services
{
    public interface ICompraGadoService : IBaseService<CompraGado>
    {
        CompraGado GetByIdComplete(int id);
        IEnumerable<CompraGado> GetAllComplete();
    }
}