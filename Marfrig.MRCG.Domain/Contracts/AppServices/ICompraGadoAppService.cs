﻿using Marfrig.MRCG.Domain.Contracts.AppServices.Base;
using Marfrig.MRCG.Domain.Models;
using System.Collections.Generic;

namespace Marfrig.MRCG.Domain.Contracts.AppServices
{
    public interface ICompraGadoAppService : IBaseAppService<CompraGado>
    {
        CompraGado GetByIdComplete(int id);
        IEnumerable<CompraGado> GetAllComplete();
    }
}