﻿using Marfrig.MRCG.Domain.Contracts.AppServices.Base;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.Domain.Contracts.AppServices
{
    public interface ICompraGadoItemAppService : IBaseAppService<CompraGadoItem>
    {

    }
}