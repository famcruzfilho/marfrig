﻿using System.Collections.Generic;

namespace Marfrig.MRCG.Domain.Contracts.Repositories.Base
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        void Create(TEntity obj);
        void Create(IEnumerable<TEntity> objs);
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int id);
        void Remove(TEntity obj);
        void Update(TEntity obj);
        void Dispose();
    }
}