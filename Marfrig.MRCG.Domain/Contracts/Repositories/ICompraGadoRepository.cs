﻿using Marfrig.MRCG.Domain.Contracts.Repositories.Base;
using Marfrig.MRCG.Domain.Models;
using System.Collections.Generic;

namespace Marfrig.MRCG.Domain.Contracts.Repositories
{
    public interface ICompraGadoRepository : IBaseRepository<CompraGado>
    {
        CompraGado GetByIdComplete(int id);
        IEnumerable<CompraGado> GetAllComplete();
    }
}