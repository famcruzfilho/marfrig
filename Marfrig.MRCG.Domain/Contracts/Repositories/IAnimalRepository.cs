﻿using Marfrig.MRCG.Domain.Contracts.Repositories.Base;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.Domain.Contracts.Repositories
{
    public interface IAnimalRepository : IBaseRepository<Animal>
    {

    }
}