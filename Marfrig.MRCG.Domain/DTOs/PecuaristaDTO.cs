﻿namespace Marfrig.MRCG.Domain.DTOs
{
    public class PecuaristaDTO
    {
        public int PecuaristaId { get; set; }
        public string Nome { get; set; }
    }
}