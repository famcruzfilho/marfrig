﻿using System;

namespace Marfrig.MRCG.Domain.DTOs
{
    public class CompraGadoDTO
    {
        public int CompraGadoId { get; set; }
        public DateTime DataEntrega { get; set; }
        public int PecuaristaId { get; set; }
        public PecuaristaDTO Pecuarista { get; set; }
    }
}