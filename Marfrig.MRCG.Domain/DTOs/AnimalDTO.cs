﻿namespace Marfrig.MRCG.Domain.DTOs
{
    public class AnimalDTO
    {
        public int AnimalId { get; set; }
        public string Descricao { get; set; }
        public string Preco { get; set; }
    }
}