﻿namespace Marfrig.MRCG.Domain.DTOs
{
    public class CompraGadoItemDTO
    {
        public int CompraGadoItemId { get; set; }
        public int Quantidade { get; set; }
        public int CompraGadoId { get; set; }
        public CompraGadoDTO CompraGado { get; set; }
        public int AnimalId { get; set; }
        public AnimalDTO Animal { get; set; }
    }
}