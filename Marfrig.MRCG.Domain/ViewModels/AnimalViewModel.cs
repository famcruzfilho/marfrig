﻿namespace Marfrig.MRCG.Domain.ViewModels
{
    public class AnimalViewModel
    {
        public int AnimalId { get; set; }
        public string Descricao { get; set; }
        public string Preco { get; set; }
    }
}