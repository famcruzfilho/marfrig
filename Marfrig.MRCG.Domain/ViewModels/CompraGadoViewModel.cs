﻿using System;

namespace Marfrig.MRCG.Domain.ViewModels
{
    public class CompraGadoViewModel
    {
        public int CompraGadoId { get; set; }
        public DateTime DataEntrega { get; set; }
        public int PecuaristaId { get; set; }
        public PecuaristaViewModel Pecuarista { get; set; }
    }
}