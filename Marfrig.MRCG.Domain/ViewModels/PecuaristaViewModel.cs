﻿namespace Marfrig.MRCG.Domain.ViewModels
{
    public class PecuaristaViewModel
    {
        public int PecuaristaId { get; set; }
        public string Nome { get; set; }
    }
}