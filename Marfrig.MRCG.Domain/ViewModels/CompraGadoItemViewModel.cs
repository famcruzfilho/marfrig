﻿namespace Marfrig.MRCG.Domain.ViewModels
{
    public class CompraGadoItemViewModel
    {
        public int CompraGadoItemId { get; set; }
        public int Quantidade { get; set; }
        public int CompraGadoId { get; set; }
        public CompraGadoViewModel CompraGado { get; set; }
        public int AnimalId { get; set; }
        public AnimalViewModel Animal { get; set; }
    }
}