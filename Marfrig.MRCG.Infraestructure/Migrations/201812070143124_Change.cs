namespace Marfrig.MRCG.Infraestructure.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Change : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pecuaristas", "Nome", c => c.String(maxLength: 100, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pecuaristas", "Nome", c => c.Int(nullable: false));
        }
    }
}