namespace Marfrig.MRCG.Infraestructure.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animais",
                c => new
                    {
                        AnimalId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100, unicode: false),
                        Preco = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.AnimalId);
            
            CreateTable(
                "dbo.ComprasGado",
                c => new
                    {
                        CompraGadoId = c.Int(nullable: false, identity: true),
                        DataEntrega = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PecuaristaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CompraGadoId)
                .ForeignKey("dbo.Pecuaristas", t => t.PecuaristaId)
                .Index(t => t.PecuaristaId);
            
            CreateTable(
                "dbo.Pecuaristas",
                c => new
                    {
                        PecuaristaId = c.Int(nullable: false, identity: true),
                        Nome = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PecuaristaId);
            
            CreateTable(
                "dbo.ComprasGadoItens",
                c => new
                    {
                        CompraGadoItemId = c.Int(nullable: false, identity: true),
                        Quantidade = c.Int(nullable: false),
                        CompraGadoId = c.Int(nullable: false),
                        AnimalId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CompraGadoItemId)
                .ForeignKey("dbo.Animais", t => t.AnimalId)
                .ForeignKey("dbo.ComprasGado", t => t.CompraGadoId)
                .Index(t => t.CompraGadoId)
                .Index(t => t.AnimalId);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ComprasGadoItens", "CompraGadoId", "dbo.ComprasGado");
            DropForeignKey("dbo.ComprasGadoItens", "AnimalId", "dbo.Animais");
            DropForeignKey("dbo.ComprasGado", "PecuaristaId", "dbo.Pecuaristas");
            DropIndex("dbo.ComprasGadoItens", new[] { "AnimalId" });
            DropIndex("dbo.ComprasGadoItens", new[] { "CompraGadoId" });
            DropIndex("dbo.ComprasGado", new[] { "PecuaristaId" });
            DropTable("dbo.ComprasGadoItens");
            DropTable("dbo.Pecuaristas");
            DropTable("dbo.ComprasGado");
            DropTable("dbo.Animais");
        }
    }
}