﻿using AutoMapper;
using Marfrig.MRCG.Domain.Contracts.Repositories;
using Marfrig.MRCG.Domain.DTOs;
using Marfrig.MRCG.Domain.Models;
using Marfrig.MRCG.Infraestructure.Data.Repositories.Base;
using System.Collections.Generic;
using System.Linq;

namespace Marfrig.MRCG.Infraestructure.Data.Repositories
{
    public class CompraGadoRepository : BaseRepository<CompraGado>, ICompraGadoRepository
    {
        public IEnumerable<CompraGado> GetAllComplete()
        {
            var dados = _context.ComprasGado.Select(x => new CompraGadoDTO
            {
                CompraGadoId = x.CompraGadoId,
                DataEntrega = x.DataEntrega,
                Pecuarista = new PecuaristaDTO
                {
                    PecuaristaId = x.Pecuarista.PecuaristaId,
                    Nome = x.Pecuarista.Nome
                }
            }).AsQueryable();

            var listaComprasGadoCompleto = Mapper.Map<IEnumerable<CompraGadoDTO>, IEnumerable<CompraGado>>(dados.ToList());

            return listaComprasGadoCompleto;
        }

        public CompraGado GetByIdComplete(int id)
        {
            var dados = _context.ComprasGado.Select(x => new CompraGadoDTO
            {
                CompraGadoId = x.CompraGadoId,
                DataEntrega = x.DataEntrega,
                Pecuarista = new PecuaristaDTO
                {
                    PecuaristaId = x.Pecuarista.PecuaristaId,
                    Nome = x.Pecuarista.Nome
                }
            }).AsQueryable();

            var compraGadoCompleto = Mapper.Map<CompraGadoDTO, CompraGado>(dados.FirstOrDefault(x => x.PecuaristaId == id));

            return compraGadoCompleto;
        }
    }
}