﻿using Marfrig.MRCG.Domain.Contracts.Repositories.Base;
using Marfrig.MRCG.Infraestructure.Data.Context;
using System.Collections.Generic;
using System.Data.Entity;

namespace Marfrig.MRCG.Infraestructure.Data.Repositories.Base
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected DataContext _context = new DataContext();

        public void Update(TEntity obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>();
        }

        public void Create(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
            _context.SaveChanges();
        }

        public void Create(IEnumerable<TEntity> objs)
        {
            foreach (TEntity obj in objs)
            {
                _context.Set<TEntity>().Add(obj);
                _context.SaveChanges();
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void Remove(TEntity obj)
        {
            _context.Set<TEntity>().Remove(obj);
            _context.SaveChanges();
        }
    }
}