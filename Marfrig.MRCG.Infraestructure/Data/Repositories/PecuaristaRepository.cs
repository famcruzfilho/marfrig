﻿using Marfrig.MRCG.Domain.Contracts.Repositories;
using Marfrig.MRCG.Domain.Models;
using Marfrig.MRCG.Infraestructure.Data.Repositories.Base;

namespace Marfrig.MRCG.Infraestructure.Data.Repositories
{
    public class PecuaristaRepository : BaseRepository<Pecuarista>, IPecuaristaRepository
    {

    }
}