﻿using Marfrig.MRCG.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Marfrig.MRCG.Infraestructure.Data.Mappings
{
    public class CompraGadoMap : EntityTypeConfiguration<CompraGado>
    {
        public CompraGadoMap()
        {
            ToTable("ComprasGado");
        }
    }
}