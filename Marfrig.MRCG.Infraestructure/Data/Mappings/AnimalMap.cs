﻿using Marfrig.MRCG.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Marfrig.MRCG.Infraestructure.Data.Mappings
{
    public class AnimalMap : EntityTypeConfiguration<Animal>
    {
        public AnimalMap()
        {
            ToTable("Animais");
        }
    }
}