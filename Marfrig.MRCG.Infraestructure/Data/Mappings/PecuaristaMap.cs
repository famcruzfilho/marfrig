﻿using Marfrig.MRCG.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Marfrig.MRCG.Infraestructure.Data.Mappings
{
    public class PecuaristaMap : EntityTypeConfiguration<Pecuarista>
    {
        public PecuaristaMap()
        {
            ToTable("Pecuaristas");
        }
    }
}