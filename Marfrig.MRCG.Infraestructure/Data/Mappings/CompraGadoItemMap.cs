﻿using Marfrig.MRCG.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Marfrig.MRCG.Infraestructure.Data.Mappings
{
    public class CompraGadoItemMap : EntityTypeConfiguration<CompraGadoItem>
    {
        public CompraGadoItemMap()
        {
            ToTable("ComprasGadoItens");
        }
    }
}