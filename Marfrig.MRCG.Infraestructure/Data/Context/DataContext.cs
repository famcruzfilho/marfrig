﻿using Marfrig.MRCG.Domain.Models;
using Marfrig.MRCG.Infraestructure.Data.Mappings;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Marfrig.MRCG.Infraestructure.Data.Context
{
    public class DataContext : DbContext
    {
        public DataContext() : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Animal> Animais { get; set; }
        public DbSet<CompraGado> ComprasGado { get; set; }
        public DbSet<CompraGadoItem> ComprasGadoItens { get; set; }
        public DbSet<Pecuarista> Pecuaristas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Properties().Where(x => x.Name == x.ReflectedType.Name + "Id").Configure(x => x.IsKey());
            modelBuilder.Properties<string>().Configure(x => x.HasColumnType("varchar"));
            modelBuilder.Properties<string>().Configure(x => x.HasMaxLength(100));
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            //Mappings
            modelBuilder.Configurations.Add(new AnimalMap());
            modelBuilder.Configurations.Add(new CompraGadoMap());
            modelBuilder.Configurations.Add(new CompraGadoItemMap());
            modelBuilder.Configurations.Add(new PecuaristaMap());
        }
    }
}
