﻿using Marfrig.MRCG.Business.Services.Base;
using Marfrig.MRCG.Domain.Contracts.Repositories;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.Business.Services
{
    public class AnimalService : BaseService<Animal>, IAnimalService
    {
        private readonly IAnimalRepository _animalRepository;

        public AnimalService(IAnimalRepository animalRepository) : base(animalRepository)
        {
            _animalRepository = animalRepository;
        }
    }
}