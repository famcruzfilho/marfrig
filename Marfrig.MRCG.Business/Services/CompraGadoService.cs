﻿using Marfrig.MRCG.Business.Services.Base;
using Marfrig.MRCG.Domain.Contracts.Repositories;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;
using System.Collections.Generic;

namespace Marfrig.MRCG.Business.Services
{
    public class CompraGadoService : BaseService<CompraGado>, ICompraGadoService
    {
        private readonly ICompraGadoRepository _compraGadoRepository;

        public CompraGadoService(ICompraGadoRepository compraGadoRepository) : base(compraGadoRepository)
        {
            _compraGadoRepository = compraGadoRepository;
        }

        public IEnumerable<CompraGado> GetAllComplete()
        {
            return _compraGadoRepository.GetAllComplete();
        }

        public CompraGado GetByIdComplete(int id)
        {
            return _compraGadoRepository.GetByIdComplete(id);
        }
    }
}