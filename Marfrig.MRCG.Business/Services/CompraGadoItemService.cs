﻿using Marfrig.MRCG.Business.Services.Base;
using Marfrig.MRCG.Domain.Contracts.Repositories;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.Business.Services
{
    public class CompraGadoItemService : BaseService<CompraGadoItem>, ICompraGadoItemService
    {
        private readonly ICompraGadoItemRepository _compraGadoItemRepository;

        public CompraGadoItemService(ICompraGadoItemRepository compraGadoItemRepository) : base(compraGadoItemRepository)
        {
            _compraGadoItemRepository = compraGadoItemRepository;
        }
    }
}