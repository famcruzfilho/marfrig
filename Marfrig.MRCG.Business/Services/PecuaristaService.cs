﻿using Marfrig.MRCG.Business.Services.Base;
using Marfrig.MRCG.Domain.Contracts.Repositories;
using Marfrig.MRCG.Domain.Contracts.Services;
using Marfrig.MRCG.Domain.Models;

namespace Marfrig.MRCG.Business.Services
{
    public class PecuaristaService : BaseService<Pecuarista>, IPecuaristaService
    {
        private readonly IPecuaristaRepository _pecuaristaRepository;

        public PecuaristaService(IPecuaristaRepository pecuaristaRepository) : base(pecuaristaRepository)
        {
            _pecuaristaRepository = pecuaristaRepository;
        }
    }
}