﻿using Marfrig.MRCG.Domain.Contracts.Repositories.Base;
using Marfrig.MRCG.Domain.Contracts.Services.Base;
using System.Collections.Generic;

namespace Marfrig.MRCG.Business.Services.Base
{
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class
    {
        private readonly IBaseRepository<TEntity> _repository;

        public BaseService(IBaseRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public void Create(TEntity obj)
        {
            _repository.Create(obj);
        }

        public void Create(IEnumerable<TEntity> objs)
        {
            _repository.Create(objs);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public TEntity GetById(int id)
        {
            return _repository.GetById(id);
        }

        public void Remove(TEntity obj)
        {
            _repository.Remove(obj);
        }

        public void Update(TEntity obj)
        {
            _repository.Update(obj);
        }
    }
}